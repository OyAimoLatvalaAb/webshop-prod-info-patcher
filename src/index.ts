import axios from 'axios';
import dotenv from 'dotenv';

import logger from './logger';
import {
    vakProducts,
    fragileProducts,
    largeProducts,
    noDeliveryProducts,
} from './data';

dotenv.config();

type ProductData = {
    EAN: string,
    salePrice: null | number,
    widthCm: null | number,
    heightCm: null | number,
    lengthCm: null | number,
    manufacturerHomepageUrl: null | number,
    unit: null | number,
    massG: null | number,
    text: null | number,
    catalogVisibility: string,
    deferredDelivery: boolean,
    isDangerous: boolean,
    isFragile: boolean,
    isLarge: boolean,
    vakAmount: null | number,
}

const blankProduct = (ean: string) => {
    return {
        EAN: ean,
        salePrice: null,
        widthCm: null,
        heightCm: null,
        lengthCm: null,
        manufacturerHomepageUrl: null,
        unit: null,
        massG: null,
        text: null,
        catalogVisibility: 'visible',
        deferredDelivery: false,
        isDangerous: false,
        isFragile: false,
        isLarge: false,
        vakAmount: null,
    };
};

const options = {
    headers: {
        Authorization: `Bearer ${process.env.JWT}`
    }
};


const createProduct = async (ean: string, data: ProductData) => {
    let req;
    try {
        req = await axios.post(`${process.env.SERVICE}/product-controller/products`, data, options);
    } catch(err) {
        logger.error(`Failed to create product ${ean}: ${err.response.status} ${err.response.data} ${JSON.stringify(err)}}`);
        throw err;
    }
    return req;
};

const patchProduct = async (ean: string, data: ProductData) => {
    let req;
    try {
        req = await axios.put(`${process.env.SERVICE}/product-controller/products/${ean}`, data, options);
    } catch(err) {
        logger.error(`Failed to patch product ${ean}: ${err.response.status} ${err.response.data} ${JSON.stringify(err)}}`);
        throw err;
    }
    return req;
};

const processProduct = async (product: {ean: string, amount: number}, transFunction: Function) => {
    const url = `${process.env.SERVICE}/product-controller/products/${product.ean}`;
    logger.info(`Getting data for product ${product.ean}`);
    let req, productData;
    try {
        req = await axios.get(url, options);
        productData = req.data;
    } catch(err) {
        if(err.response.status == 404) {
            logger.debug(`Product ${product.ean} does not exist. Creating blank product`);
            try {
                productData = blankProduct(product.ean);
                req = await createProduct(product.ean, productData);
            } catch(err) {
                logger.error(JSON.stringify({err, req}));
                throw err;
            }
        } else {
            logger.error(JSON.stringify({err, req}));
            throw err;
        }
    }
    logger.debug(`Got data ${JSON.stringify(productData)}`);
    transFunction(product, productData);
    logger.info(`Patching product ${product.ean}`);
    patchProduct(product.ean, productData);
    logger.info(`Done patching product ${product.ean}`);
};

const main = async () => {

    const dangerousProductTransFn = (product: {ean: string, amount: number}, productData: ProductData) => {
        productData.isDangerous = true;
        productData.vakAmount = product.amount;
    };
    const fragileProductTransFn = (product: {ean: string, amount: number}, productData: ProductData) => {
        productData.isFragile = true;
        productData.vakAmount = product.amount;
    };
    const largeProductTransFn = (product: {ean: string, amount: number}, productData: ProductData) => {
        productData.isLarge = true;
        productData.vakAmount = product.amount;
    };

    // Yes, I know that doing a Promise.all on these would be better, but as performance isn't critical I'm
    // doing them one by one.
    for(const vakProduct of vakProducts) {
        await processProduct(vakProduct, dangerousProductTransFn);
    }
    for(const fragileProduct of fragileProducts) {
        await processProduct(fragileProduct, fragileProductTransFn);
    }
    for(const largeProduct of largeProducts) {
        await processProduct(largeProduct, largeProductTransFn);
    }
    for(const noDeliveryProduct of noDeliveryProducts) {
        await processProduct(noDeliveryProduct, dangerousProductTransFn);
    }
};


main().catch(err => {
    logger.error(err.message);
    throw err;
});
