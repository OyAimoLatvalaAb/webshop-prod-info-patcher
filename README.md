# Webshop product info patcher

## What's this

A utility to aid in patching products found at [Webshop Product Info Backend](https://bitbucket.org/OyAimoLatvalaAb/webshop-prod-info-backend/src).

## How do I use it?

Pretty simple, edit files ending with `.example` and drop the `.example` extension. Then do `yarn install && yarn dev`.
